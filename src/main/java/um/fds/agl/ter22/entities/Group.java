package um.fds.agl.ter22.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
@Entity
public class Group {

    private @Id @GeneratedValue Long id;

    private String Name;


    public Group(String Name){
        this.Name = Name;
    }

    public Group(Long id, String Name){
        this.Name = Name;
        this.id = id;
    }

    public Group(){    }

    public Long getId() { return this.id; }

    public void setId(Long id){ this.id=id; }

    public String getName(){return this.Name;}

    public void setName(String name){this.Name=name;}


    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Group)) return false;
        if (!super.equals(o)) return false;

        Group group = (Group) o;

        if (!getId().equals(group.getId())) return false;
        return getName().equals(group.getName());
    }

    public int hashCode() {
        int result = getId().hashCode();
        result = 31 * result + getName().hashCode();
        return result;
    }


}
