package um.fds.agl.ter22.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Subject {

    private @Id @GeneratedValue Long id;

    private String Name;

    private String Desc;

    private String Teacher;

    private String Teacher2;

    public Subject(String Name,String desc ){
        this.Name = Name;
        this.Desc = desc;
    }

    public Subject(String Name,String desc, String Teacher,String Teacher2 ){
        this.Name = Name;
        this.Desc = desc;
        this.Teacher = Teacher;
        this.Teacher2 = Teacher2;
    }

    public Subject(){    }

    public Long getId() { return this.id; }

    public void setId(Long id){ this.id=id; }

    public String getName(){return this.Name;}

    public void setName(String name){this.Name=name;}

    public String getDesc(){return this.Desc;}

    public void setDesc(String desc){this.Desc=desc;}

    public String getTeacher(){return this.Teacher;}

    public void setTeacher(String teacher){this.Teacher=teacher;}

    public String getTeacher2(){return this.Teacher2;}

    public void setTeacher2(String teacher){this.Teacher2=teacher;}

    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Teacher)) return false;
        if (!super.equals(o)) return false;

        Subject subject = (Subject) o;

        return getTeacher() != null ? getTeacher().equals(subject.getTeacher()) : subject.getTeacher() == null;
    }


    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (getTeacher() != null ? getTeacher().hashCode() : 0);
        return result;
    }


}
