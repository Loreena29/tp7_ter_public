package um.fds.agl.ter22.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.security.access.prepost.PreAuthorize;
import um.fds.agl.ter22.entities.Group;

public interface GroupRepository<T extends Group>
        extends CrudRepository<T, Long> {

    @PreAuthorize("hasRole('ROLE_MANAGER')"+"|| hasRole('ROLE_TEACHER')")
    Group save(@Param("group") Group group);

    @Override
    @PreAuthorize("hasRole('ROLE_MANAGER')"+"|| hasRole('ROLE_TEACHER')")
    void delete(@Param("group")Group group);

    @PreAuthorize("hasRole('ROLE_MANAGER')"+"|| hasRole('ROLE_TEACHER')")
    void deleteById(@Param("id") Long id);


}
