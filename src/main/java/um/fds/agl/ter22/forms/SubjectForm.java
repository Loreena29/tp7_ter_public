package um.fds.agl.ter22.forms;

public class SubjectForm {
    private String name;
    private long id;
    private String desc;

    private String teacher;

    private String teacher2;

    public SubjectForm(long id, String Name, String Desc, String Teacher,String Teacher2) {
        this.name = Name;
        this.desc = Desc;
        this.id = id;
        this.teacher=Teacher;
        this.teacher2=Teacher2;
    }

    public SubjectForm() {

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name){this.name=name;}

    public String getDesc() { return desc; }

    public void setDesc(String desc){this.desc=desc;}

    public String getTeacher(){return this.teacher;}

    public void setTeacher(String teacher){this.teacher=teacher;}

    public String getTeacher2(){return this.teacher2;}

    public void setTeacher2(String teacher){this.teacher2=teacher;}
}
