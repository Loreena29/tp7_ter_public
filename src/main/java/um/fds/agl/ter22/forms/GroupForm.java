package um.fds.agl.ter22.forms;


public class GroupForm {
    private String name;
    private Long id;

    public GroupForm(Long id, String name) {
        this.name = name;
        this.id = id;
    }

    public GroupForm() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name){this.name=name;}


}
