package um.fds.agl.ter22.controllers;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import static org.mockito.Mockito.times;

import um.fds.agl.ter22.entities.TERManager;
import um.fds.agl.ter22.entities.Teacher;
import um.fds.agl.ter22.services.TeacherService;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.assumingThat;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class TeacherControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private TeacherService mockTeacherService;


    @Captor
    ArgumentCaptor<Teacher> captor = ArgumentCaptor.forClass(Teacher.class);

    @WithMockUser(username = "Chef", roles = "MANAGER")
    @Test
    void addTeacherGet() throws Exception {
        MvcResult result = mvc.perform(get("/addTeacher"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("text/html;charset=UTF-8"))
                .andExpect(view().name("addTeacher"))
                .andReturn();
    }

    @WithMockUser(username = "Chef", roles = "MANAGER")
    @Test
    void addTeacherPost() throws Exception {
        Teacher teacher = new Teacher(10l, "Anne-Marie", "Kermarrec", "mdp2", new TERManager("Le", "Chef", "mdp", "ROLE_MANAGER"), "ROLE_TEACHER");
        when(mockTeacherService.saveTeacher(any(Teacher.class))).thenReturn(teacher);

        mvc.perform(post("/addTeacher")
                        .param("firstName", "Anne-Marie")
                        .param("lastName", "Kermarrec")
                        .param("id", "10")
                ).andExpect(status().is3xxRedirection())
                .andReturn();
        verify(mockTeacherService).saveTeacher(captor.capture());
        assertEquals(captor.getValue().getId(), teacher.getId());
        assertEquals(captor.getValue().getFirstName(), teacher.getFirstName());
        assertEquals(captor.getValue().getLastName(), teacher.getLastName());
    }

    @Test
    @WithMockUser(username = "Chef", roles = "MANAGER")
    void addTeacherPostExistingTeacher() throws Exception {
        Teacher teacher = new Teacher(15l, "Anne-Marie", "Kermarrec", "mdp2", new TERManager("Le", "Chef", "mdp", "ROLE_MANAGER"), "ROLE_TEACHER");
        Teacher teacherUpdate = new Teacher(15l, "John", "Doe",  "mdp2", new TERManager("Le", "Chef", "mdp", "ROLE_MANAGER"), "ROLE_TEACHER");

        when(mockTeacherService.saveTeacher(any(Teacher.class))).thenReturn(teacher, teacherUpdate);

        mvc.perform(post("/addTeacher")
                        .param("firstName", "Anne-Marie")
                        .param("lastName", "Kermarrec")
                        .param("id", "15")
                ).andExpect(status().is3xxRedirection())
                .andReturn();

        verify(mockTeacherService,times(1)).saveTeacher(captor.capture());
        assertEquals(captor.getValue().getId(), teacher.getId());
        assertEquals(captor.getValue().getFirstName(), teacher.getFirstName());
        assertEquals(captor.getValue().getLastName(), teacher.getLastName());


        mvc.perform(post("/addTeacher")
                        .param("firstName", "John")
                        .param("lastName", "Doe")
                        .param("id", "15")
                ).andExpect(status().is3xxRedirection())
                .andReturn();

        verify(mockTeacherService,times(2)).saveTeacher(captor.capture());
        assertEquals(captor.getValue().getId(), teacherUpdate.getId());
        assertEquals(captor.getValue().getFirstName(), teacherUpdate.getFirstName());
        assertEquals(captor.getValue().getLastName(), teacherUpdate.getLastName());
    }
}