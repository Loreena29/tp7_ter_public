package um.fds.agl.ter22.seleniumTests;

import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class AddingTeacherTest extends BaseForTests{


    @Test
    void scenario1(){
        login("Turing", "turing");
        WebElement listTeacherButton = driver.findElement(By.linkText("Teachers List"));
        click(listTeacherButton);
        WebElement addTeacherButton = driver.findElement(By.linkText("Add Teachers"));
        click(addTeacherButton);
        assertEquals(driver.getTitle(),"Error");
    }

    @Test
    void scenario2() throws IOException {
        login("Chef", "mdp");
        WebElement listTeacherButton = driver.findElement(By.linkText("Teachers List"));
        click(listTeacherButton);
        WebElement addTeacherButton = driver.findElement(By.linkText("Add Teachers"));
        click(addTeacherButton);

        WebElement firstNameTest = driver.findElement(By.id("firstName"));
        WebElement lastNameTest = driver.findElement(By.id("lastName"));
        WebElement createButton = driver.findElement(By.cssSelector("[type=submit]"));
        write(firstNameTest, "TeacherFirstNameForTest");
        write(lastNameTest, "TeacherLastNameForTest");
        click(createButton);

        assertTrue(driver.getTitle().contains("Teacher List"));
        assertTrue(driver.getPageSource().contains("TeacherFirstNameForTest"));

        screenshot("AddTeacher_Scenario2");
    }

}
