package um.fds.agl.ter22.seleniumTests;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.time.Duration;


public class BaseForTests {
    static WebDriver driver;
    String serverAdress="http://localhost:8080";

    protected WebDriverWait wait;


    @BeforeAll //Appellée à la création de la classe
    static void setup() {
        WebDriverManager.firefoxdriver().setup();
    }
    // met en place "firefox driver" qui permet de controler firefox
    @BeforeEach //Appellée avant chaque test
    void init() {
        driver = WebDriverManager.firefoxdriver().create();
        Duration d = Duration.ofSeconds(1);
        wait = new WebDriverWait(driver, d);
    } // création du webdriver, que l'on stock dans "driver", et défini le temps d'attente de "wait"


    public void waitElement(WebElement element) {
        wait.until(ExpectedConditions.visibilityOf(element));
    }
    // Méthode qui attend qu'un élément soir visible

    public void click(WebElement element) {
        waitElement(element);
        element.click();
    }
    // Méthode qui permet de cliquer sur "element"

    public void write(WebElement element, String text) {
        waitElement(element);
        element.sendKeys(text);
    }
    // Méthode qui écrit "text" dans "element"

    public String read(WebElement element) {
        waitElement(element);
        return element.getText();
    }
    // Méthode qui lit ce qu'il y a dans "element"

    public  void screenshot(String fileName)
            throws IOException
    {
        File File = ((TakesScreenshot)driver)
                .getScreenshotAs(OutputType.FILE);
        String home = System.getenv("HOME");
        FileUtils.copyFile(File,
                new File(home+"/tmp/selenium/"+ this.getClass().getName()+"-"+fileName + ".jpeg"));
    }
    // Méthode qui permet de faire une capture d'écran et de le ranger dans un endroit spécifique

    void login(String userName, String password)  {
        driver.get(serverAdress+"/login");
        WebElement userNameField = driver.findElement(By.id("username"));
        WebElement passwordField = driver.findElement(By.id("password"));
        WebElement loginButton = driver.findElement(By.cssSelector("[type=submit]"));
        write(userNameField, userName);
        write(passwordField, password);
        click(loginButton);
    }
    // Seule méthode spécifique à notre application. Elle permet de se connecter
}
